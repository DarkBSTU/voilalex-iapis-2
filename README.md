# Лоис. Lab 2

## Build

At first, create `build` folder. 
```bash
mkdir build
cd build
```

Then build it.
```bash
cmake ..
make
```


## Running

In the `build` directory:
```bash
./lab_2_main
```