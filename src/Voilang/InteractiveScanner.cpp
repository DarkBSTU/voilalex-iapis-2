#include <iostream>

#include "InteractiveScanner.h"

bool InteractiveScanner::ok() const {
    return !m_source->bad();
}

InteractiveScanner::InteractiveScanner()
        : InstructionScanner(static_cast<std::shared_ptr<std::istream>>(&std::cin)) {}
