#include "Voilang.h"

int main (int argc, char** argv)
{
    FILE *log_file = fopen("ilyanguage.log", "a+");
    log_set_level(LOG_INFO);
    log_set_fp(log_file);
    IlyaMain(argc, argv);
    fclose(log_file);
}