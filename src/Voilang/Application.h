#pragma once

#include <memory>
#include <vector>

#include "InstructionScanner.h"
#include "LexicalAnalyzer.h"
#include "SyntaxAnalyzer.h"
#include "Token.h"

class Application
{
protected:
    std::shared_ptr<InstructionScanner> m_instructionScanner;
    std::shared_ptr<LexicalAnalyzer> m_lexicalAnalyzer;
    std::vector<std::shared_ptr<SyntaxAnalyzer>> m_syntaxAnalyzers;
public:
    Application(
        std::shared_ptr<InstructionScanner>  instructionScanner,
        std::shared_ptr<LexicalAnalyzer>  lexicalAnalyzer,
        const std::shared_ptr<SyntaxAnalyzer>& syntaxAnalyzer = nullptr
    );

    /**
     * @brief Set the InstrucationScanner object
     * 
     * @param instructionScanner 
     */
    void setInstrucationScanner(const std::shared_ptr<InstructionScanner>& instructionScanner);

    /**
     * @brief Set the LexicalAnalyzer object
     * 
     * @param lexicalAnalyzer 
     */
    void setLexicalAnalyzer(const std::shared_ptr<LexicalAnalyzer>& lexicalAnalyzer);


    /**
     * @brief Add syntax analyzer to the application
     * 
     * @param syntaxAnalyzer syntax analyzer to add
     */
    void addSyntaxAnalyzer(const std::shared_ptr<SyntaxAnalyzer>& syntaxAnalyzer);

    /**
     * @brief main method when lifecycle is implemented
     * 
     * @param argc given by user
     * @param argv given by user
     * @return int like return in main function
     */
    int operator()(int argc, char ** argv);
protected:
    /**
     * @brief This method is part of application lifecycle and might be implemented by user
     * 
     * @param argc  given by user when operator() is called
     * @param argv  given by user when operator() is called
     * @return int  return value processed in operator() 
     */
    virtual int run (int argc, char ** argv);

    /**
     * @brief Hook to manipulate lexical data
     * 
     * @param lexicalData 
     */
    virtual void useLexicalData(LexicalData& lexicalData);

    /**
     * @brief Hook to manipulate syntax data
     * 
     * @param syntaxData 
     */
    virtual void useSyntaxData(SyntaxData& syntaxData);
};