#include "Application.h"

#include <utility>
#include "../Voilang/vendor/log.h"
#include "Token.h"
#include "Utils.h"
#include "../Voilang/exception/AnalyzersNotFound.h"
#include "exception/LexicalError.h"
#include "exception/SyntaxError.h"

Application::Application(
    std::shared_ptr<InstructionScanner> instructionScanner,
    std::shared_ptr<LexicalAnalyzer> lexicalAnalyzer,
    const std::shared_ptr<SyntaxAnalyzer> &syntaxAnalyzer)
    : m_instructionScanner(std::move(instructionScanner)),
      m_lexicalAnalyzer(std::move(lexicalAnalyzer)),
      m_syntaxAnalyzers(1, syntaxAnalyzer)
    {}

void Application::addSyntaxAnalyzer(const std::shared_ptr<SyntaxAnalyzer> &syntaxAnalyzer)
{
    m_syntaxAnalyzers.emplace_back(syntaxAnalyzer);
}


int Application::run(int argc, char ** argv) 
{
    return 0;
}

int Application::operator()(int argc, char ** argv) 
{
    if (m_syntaxAnalyzers.empty())
        throw AnalyzersNotFound("There is no analyzers found.");

    std::vector<Symbol> symbols;
    LexicalData lexicalData;
    SyntaxData syntaxData;
    while (m_instructionScanner->ok()) {

        // Read an instruction
        (*m_instructionScanner) >> symbols;

        // Tokenize symbols
        log_info("Perform lexical analysis...");
        try {
            lexicalData = (*m_lexicalAnalyzer)(symbols);
        } catch (LexicalError& e) {
            log_error("Lexical error occurred.");
            continue;
        }
        log_info("Lexical analysis done. The input is lexically correct.");
        useLexicalData(lexicalData);

        // Analyze tokens
        log_info("Perform syntax analysis...");
        try {
            syntaxData = (*m_syntaxAnalyzers.back())(lexicalData);
        } catch (SyntaxError& e) {
            log_error("Syntax error occurred.");
            continue;
        }
        log_info("Lexical analysis done. The input is syntactically correct.");
        useSyntaxData(syntaxData);
    }
    
    return 0;
}

void Application::useLexicalData(LexicalData &lexicalData) {

}

void Application::useSyntaxData(SyntaxData &syntaxData) {

}
