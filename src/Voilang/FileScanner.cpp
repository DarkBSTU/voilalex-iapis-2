#include <fstream>

#include "FileScanner.h"

bool FileScanner::ok() const {
    return !m_source->bad() and !m_source->eof();
}

FileScanner::FileScanner(const std::string &file)
    : InstructionScanner(static_cast<std::shared_ptr<std::istream>>(new std::ifstream(file))) {
}
