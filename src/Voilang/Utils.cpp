#include "Utils.h"
#include "Token.h"

#ifdef TOKEN_MERGER && OLD_SYNTAX_DATA

TokenMerger::TokenMerger() 
{}

TokenMerger::TokenRow TokenMerger::merge(const TokenMatrix& tokenMatrix) const 
{
    return merge(tokenMatrix, firstOccurrenceMergePolicy);
}

TokenMerger::TokenRow TokenMerger::merge(const TokenMatrix& tokenMatrix, FirstOccurrenceMergePolicy) const
{
    SyntaxData mergedTokenRow = {
        .symbols = tokenMatrix.front().symbols,
        .tokens = tokenMatrix.front().tokens
    };

    for (int i = 0; i < tokenMatrix.size(); i++)
        for(int tokenIdx = 0; tokenIdx < tokenMatrix[i].tokens.size(); tokenIdx ++)
        {
            bool isPlaceForToken = true;
            auto& token = tokenMatrix[i].tokens[tokenIdx];
            for(int i = token.beginIdx; i < token.endIdx; i++)
                if (mergedTokenRow.symbols[i].isRelevant == true)
                    isPlaceForToken = false;

            if (isPlaceForToken)
            {
                int idxToInsert = 0;
                for(;idxToInsert < mergedTokenRow.tokens.size(); i++)
                    if (mergedTokenRow.tokens[idxToInsert].endIdx <= token.beginIdx)
                    {
                        idxToInsert++;
                        break;
                    }
                mergedTokenRow.tokens.insert(mergedTokenRow.tokens.begin() + idxToInsert, token);
                for (int i = token.beginIdx; i < token.endIdx; i++) 
                    mergedTokenRow.symbols[i].token = &mergedTokenRow.tokens[idxToInsert];
            }
        }
    return mergedTokenRow;
} 
TokenMerger::TokenRow TokenMerger::merge(const TokenMatrix& tokenMatrix, LastOccurrenceMergePolicy) const
{
    SyntaxData mergedTokenRow = {
        .symbols = tokenMatrix.back().symbols,
        .tokens = tokenMatrix.back().tokens
    };

    for (int i = 0; i < tokenMatrix.size(); i++)
        for(int tokenIdx = tokenMatrix[i].tokens.size() - 1; tokenIdx >= 0; tokenIdx ++)
        {
            bool isPlaceForToken = true;
            auto& token = tokenMatrix[i].tokens[tokenIdx];
            for(int i = token.beginIdx; i < token.endIdx; i++)
                if (mergedTokenRow.symbols[i].isRelevant == true)
                    isPlaceForToken = false;

            if (isPlaceForToken)
            {
                int idxToInsert = 0;
                for(;idxToInsert < mergedTokenRow.tokens.size(); i++)
                    if (mergedTokenRow.tokens[idxToInsert].endIdx <= token.beginIdx)
                    {
                        idxToInsert++;
                        break;
                    }
                mergedTokenRow.tokens.insert(mergedTokenRow.tokens.begin() + idxToInsert, token);
                for (int i = token.beginIdx; i < token.endIdx; i++) 
                    mergedTokenRow.symbols[i].token = &mergedTokenRow.tokens[idxToInsert];
            }
        }
    return mergedTokenRow;
}

TokenMerger& TokenMerger::getInstance() 
{
    if (instance == nullptr)
        instance = new TokenMerger();
    return *instance;
}

TokenMerger *TokenMerger::instance = nullptr;

#endif

std::string toString(const std::vector<Symbol>& symbols)
{
    std::string result;
    std::for_each(symbols.begin(), symbols.end(), [&](Symbol symbol) {
        result += symbol.value;
    });
    return result;
}

std::string toString(TokenType tokenType)
{
    if (tokenStringMapping.find(tokenType) == tokenStringMapping.end())
        return "UNKNOWN_TOKEN";
    return tokenStringMapping[tokenType];
}