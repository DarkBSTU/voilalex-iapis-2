#pragma once

#include <vector>
#include <string>

#include "Token.h"

class LexicalAnalyzer
{
public:
    virtual LexicalData operator()(const std::vector<Symbol> &text) = 0;
};