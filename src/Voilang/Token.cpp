#include "Token.h"

std::map<int, std::string> tokenStringMapping = {
    {TokenType::IDENTIFIER, "IDENTIFIER"},
    {TokenType::ENDRULE, "ENDRULE"},
    {TokenType::STRING_CONSTANT, "STRING_CONSTANT"},
    {TokenType::INTEGER_CONSTANT, "INTEGER_CONSTANT"},
    {TokenType::FLOAT_CONSTANT, "FLOAT_CONSTANT"},
    {TokenType::CHAR_CONSTANT, "CHAR_CONSTANT"},
    {TokenType::START_ARGUMENT_LIST, "START_ARGUMENT_LIST"},
    {TokenType::END_ARGUMENT_LIST, "END_ARGUMENT_LIST"},
    {TokenType::ARGUMENTS_SEPARATOR, "ARGUMENTS_SEPARATOR"}
};