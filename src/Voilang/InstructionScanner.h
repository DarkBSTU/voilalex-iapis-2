#pragma once 

#include <iostream>
#include <vector>
#include <memory>

#include "Token.h"

class InstructionScanner
{
protected:
    std::shared_ptr<std::istream> m_source;
public:
    InstructionScanner(std::shared_ptr<std::istream> source);
    virtual InstructionScanner& operator >> (std::vector<Symbol>& buffer) = 0;
    virtual bool ok() const = 0;
};