#pragma once

#include <vector>
#include <string>
#include <algorithm>

#include "Token.h"

#ifdef TOKEN_MERGER

class TokenMerger 
{
    static TokenMerger* instance;
    TokenMerger();
public:
    // Tags to identify policy to use
    // while splitting

    static struct MergePolicyBase{} mergePolicyBase;

    static struct FirstOccurrenceMergePolicy : MergePolicyBase{} firstOccurrenceMergePolicy;

    static struct LastOccurrenceMergePolicy : MergePolicyBase{} lastOccurrenceMergePolicy; 
public:
    typedef SyntaxData TokenRow;
    typedef std::vector<TokenRow> TokenMatrix;
public:
    static TokenMerger& getInstance();
    TokenRow merge(const TokenMatrix& tokenMatrix) const; 
    TokenRow merge(const TokenMatrix& tokenMatrix, FirstOccurrenceMergePolicy) const; 
    TokenRow merge(const TokenMatrix& tokenMatrix, LastOccurrenceMergePolicy) const;
};

#endif

std::string toString(const std::vector<Symbol>& symbols);
std::string toString(TokenType tokenType);