#pragma once

#include <vector>

#include "Token.h"


class SyntaxAnalyzer
{
public:
    virtual SyntaxData operator()(const LexicalData& text) = 0;
};