#pragma once

#include <string>
#include <vector>
#include <map>

enum TokenType
{
    EMPTY_TOKEN = 0,
    IDENTIFIER,
    ENDRULE,
    STRING_CONSTANT,
    INTEGER_CONSTANT,
    FLOAT_CONSTANT,
    CHAR_CONSTANT,
    START_ARGUMENT_LIST,
    END_ARGUMENT_LIST,
    ARGUMENTS_SEPARATOR,
    KEYWORD,
    BINARY_OPERATOR,
    ASSIGNMENT
};

extern std::map<int, std::string> tokenStringMapping;

struct Token
{
    // If analyzer didn't recognize
    // the token then it will isRecognized 
    // will be false. It is usefull in case
    // when one analyzer analyze only one 
    // piece of tokens and other analyze other 
    // piece. Then the tokens will be merged.
    bool isRecognized = false;

    // Concrete value of the token.
    std::string value;

    // Type of the token. 
    // See TokenType for more details.
    TokenType type;

    // Indicies of the beginning
    // and the ending symbol
    int beginIdx;
    int endIdx;
};

enum SymbolType
{
    NEWLINE = 0,
    BLANK, 
    NUMERIC,
    ALPHA,
    OTHER,
    SPECIAL
};

struct Symbol
{
    // Wether this symbol is 
    // relevant ot the analyzer.
    bool isRelevant = true;

    // Type of the symbol.
    // See SymbolType for more details.
    SymbolType type;

    // Value of the symbol itself
    char value;

    // Index of the token to which 
    // this symbol belong. -1 if the 
    // symbol don't belong to any token.
    Token* token = nullptr;
};

struct SyntaxData 
{};

struct LexicalData
{
    std::vector<Symbol> symbols;
    std::vector<Token> tokens;
};
