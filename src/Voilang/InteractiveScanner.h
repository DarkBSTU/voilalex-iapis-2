#pragma once

#include <iostream>

#include "InstructionScanner.h"

class InteractiveScanner : public InstructionScanner {
public:
    InteractiveScanner();

    bool ok() const override;
};