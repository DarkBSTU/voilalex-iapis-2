#pragma once

#include <fstream>

#include "InstructionScanner.h"

class FileScanner : public InstructionScanner
{
public:
    FileScanner(const std::string& file);
    bool ok() const override;
};