#pragma once

#include "BadTokenFormat.h"

class BadOperatorToken : public BadTokenFormat
{
public:
    using BadTokenFormat::BadTokenFormat;
};