#pragma once

#include <exception>

class AnalyzersNotFound : public std::runtime_error
{
public:
    using std::runtime_error::runtime_error;
};