#pragma once

#include "LexicalError.h"

class BadTokenFormat : public LexicalError
{
public:
    using LexicalError::LexicalError;
};