#pragma once

#include "Application.h"

// base class for all scanners
#include "InstructionScanner.h"

// scanner for interactive shell
#include "InteractiveScanner.h"

// scanner for files
#include "FileScanner.h"

// base class for all the lexical analyzers
#include "LexicalAnalyzer.h"

// base class for all the syntax analyzers
#include "SyntaxAnalyzer.h"

// utilities
#include "Utils.h"

// vendor libraries
#include "vendor/log.h"

// exceptions
#include "exception/AnalyzersNotFound.h"
#include "exception/BadTokenFormat.h"
#include "exception/BadOperatorToken.h"
#include "exception/SyntaxError.h"
#include "exception/LexicalError.h"

int IlyaMain(int argc, char ** argv);

int main (int argc, char** argv);