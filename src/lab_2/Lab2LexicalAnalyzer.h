#pragma once

#include <vector>
#include <string>
#include <set>

#include "../Voilang/Token.h"
#include "../Voilang/LexicalAnalyzer.h"

class Lab2LexicalAnalyzer : public LexicalAnalyzer
{
    static std::set<std::string> keyWords;
    static std::set<char> specialSymbols;
public:
    LexicalData operator()(const std::vector<Symbol> &text) override;

    static std::set<std::string> getKeyWords();

    static std::set<char> getSpecialSymbols();
};
