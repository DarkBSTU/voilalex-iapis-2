#include <boost/algorithm/cxx11/all_of.hpp>
#include <boost/algorithm/cxx11/any_of.hpp>

#include "Lab2Scanner.h"
#include "Lab2LexicalAnalyzer.h"

using boost::algorithm::all_of;
using boost::algorithm::any_of;

InstructionScanner &Lab2Scanner::operator>>(std::vector<Symbol> &buffer) {
    buffer.clear();
    Symbol currentSymbol;

    bool isOk = ok();
    while(isOk) {
        currentSymbol.value = m_source->get();
        currentSymbol.isRelevant = true;

        if (isdigit(currentSymbol.value))
            currentSymbol.type = SymbolType::NUMERIC;
        else if (isalpha(currentSymbol.value))
            currentSymbol.type = SymbolType::ALPHA;
        else if (isblank(currentSymbol.value))
        {
            currentSymbol.type = SymbolType::BLANK;
            currentSymbol.isRelevant = false;
        }
        else if (currentSymbol.value == '\n')
        {
            currentSymbol.type = SymbolType::NEWLINE;
            currentSymbol.isRelevant = false;
        }
        else if (Lab2LexicalAnalyzer::getSpecialSymbols().count(currentSymbol.value))
            currentSymbol.type = SymbolType::SPECIAL;
        else
            currentSymbol.type = SymbolType::OTHER;

        // ================
        // Break conditions
        // ================
        if (currentSymbol.value == ';')
            isOk = false;

        buffer.push_back(currentSymbol);

        isOk = ok() && isOk;
    }

    return *this;
}
