#pragma once

#include "../Voilang/SyntaxAnalyzer.h"
#include "../Voilang/Token.h"

class Lab2SyntaxAnalyzer : public SyntaxAnalyzer
{
public:
    SyntaxData operator()(const LexicalData& text) override;
};
