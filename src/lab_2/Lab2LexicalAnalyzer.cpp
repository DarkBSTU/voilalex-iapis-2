#include "Lab2LexicalAnalyzer.h"
#include "../Voilang/exception/BadTokenFormat.h"
#include <stack>
#include <set>
#include <boost/format.hpp>

std::set<std::string> Lab2LexicalAnalyzer::keyWords = {};

std::set<char> Lab2LexicalAnalyzer::specialSymbols = {
    '(',
    ')',
    '.',
    ',',
    ';',
    '"',
    '\''    
};

Token readStringToken(const std::vector<Symbol> &symbols, size_t &startPosition)
{
    if (symbols[startPosition].value != '"')
        throw BadTokenFormat("String token should start with \" symbol.");

    Token result;
    result.isRecognized = true;
    result.value = "";
    result.type = TokenType::STRING_CONSTANT;
    result.beginIdx = startPosition;

    int currentPosition = startPosition + 1;
    startPosition++;
    while (symbols.at(currentPosition).value != '"')
    {
        result.value += symbols.at(currentPosition).value;
        currentPosition++;
        startPosition++;
        if (currentPosition == symbols.size())
            throw BadTokenFormat("String token should end with \" symbol");
    }
    result.endIdx = currentPosition;
    return result;
}

Token readCharToken(const std::vector<Symbol>& symbols, size_t& startPosition)
{
    if (symbols[startPosition].value != '\'' || symbols[startPosition + 2].value != '\'')
        throw BadTokenFormat("Bad character token format. Format is '<c>' where <c> is a character.");
    Token result;
    result.isRecognized = true;
    result.value = (boost::format("%1%") % symbols[startPosition + 1].value).str();
    result.beginIdx = startPosition;
    result.endIdx = startPosition + 3;
    result.type = TokenType::CHAR_CONSTANT;
    startPosition += 2;
    return result;
}

Token readOperatorToken(const std::vector<Symbol> &symbols, size_t &startPosition)
{
    Token result;
    result.isRecognized = true;
    result.value = "" + symbols.at(startPosition).value;
    result.type = TokenType::BINARY_OPERATOR;
    result.beginIdx = startPosition;
    result.endIdx = startPosition + 1;
    switch (symbols.at(startPosition).value)
    {
    case '+':
    case '-':
    case '*':
    case '/':
        break;
    case '!':
    case '=':
    case '<':
    case '>':
    {
        Symbol nextSymbol = symbols.at(startPosition + 1);
        if (nextSymbol.value == '=')
        {
            result.value += nextSymbol.value;
            result.endIdx++;
        }
        startPosition++;
    }
    break;

    default:
        throw BadTokenFormat(std::string("Special symbol ") + symbols.at(startPosition).value + " is not an operator.");
    }
    return result;
}

Token readNumericToken(const std::vector<Symbol> &symbols, size_t &startPosition)
{
    Token result;
    result.isRecognized = true;
    result.type = TokenType::INTEGER_CONSTANT;
    result.beginIdx = startPosition;

    if (symbols.at(startPosition).type != SymbolType::NUMERIC)
        throw BadTokenFormat("Numeric should start with number.");

    size_t currentPosition = startPosition;
    bool dotPassed = false;
    while (symbols.at(currentPosition).type == SymbolType::NUMERIC ||
           (!dotPassed && symbols.at(currentPosition).value == '.'))
    {
        if (symbols.at(currentPosition).value == '.')
        {
            dotPassed = true;
            result.type = TokenType::FLOAT_CONSTANT;
        }
        result.value += symbols.at(currentPosition).value;
        currentPosition++;
        startPosition++;
        if (currentPosition == symbols.size())
            break;
    }
    if (symbols.at(currentPosition).type == SymbolType::ALPHA)
        throw BadTokenFormat("Bad numeric format.");

    // Set startPosition on the last symbol
    startPosition--;
    result.endIdx = currentPosition;

    return result;
}

Token readIdentifierOrKeyword(const std::vector<Symbol> &symbols, size_t &startPosition)
{
    Token result;
    result.isRecognized = true,
    result.type = TokenType::IDENTIFIER,
    result.beginIdx = startPosition;
    size_t currentPosition = startPosition;
    while (symbols.at(currentPosition).type == SymbolType::NUMERIC ||
           symbols.at(currentPosition).type == SymbolType::ALPHA ||
           symbols.at(currentPosition).value == '_')
    {
        result.value += symbols.at(currentPosition).value;
        currentPosition++;
        startPosition++;
        if (currentPosition == symbols.size())
            break;
    }

    // Set startPosition on the last symbol
    startPosition--;
    result.endIdx = currentPosition;
    if (Lab2LexicalAnalyzer::getKeyWords().count(result.value) != 0)
        result.type = TokenType::KEYWORD;
    return result;
}

Token buildOneSymbolToken(TokenType type, Symbol symbol, size_t beginIdx)
{
    Token token;
    token.isRecognized = true;
    token.value = (boost::format("%1%") % symbol.value).str();
    token.type = type;
    token.beginIdx = beginIdx;
    token.endIdx = beginIdx + 1;
    return token;
}

LexicalData Lab2LexicalAnalyzer::operator()(const std::vector<Symbol> &symbols)
{
    LexicalData lexicalData{
            .symbols = symbols
    };

    size_t currentTokenIdx = 0;
    Token currentToken;

    auto pushCurrentToken = [&](int idx) {
        if (!currentToken.endIdx)
            currentToken.endIdx = idx;
        lexicalData.tokens.push_back(currentToken);
        currentTokenIdx++;
        currentToken = Token();
        currentToken.beginIdx = idx;
    };
    size_t i = 0;
    for (; i < lexicalData.symbols.size(); i++)
    {
        Symbol &currentSymbol = lexicalData.symbols.at(i);
        if (!currentSymbol.isRelevant)
            continue;

        switch (currentSymbol.type)
        {
        case SymbolType::NUMERIC:
            currentToken = readNumericToken(lexicalData.symbols, i);
            pushCurrentToken(i);
            break;

        case SymbolType::ALPHA:
            currentToken = readIdentifierOrKeyword(lexicalData.symbols, i);
            pushCurrentToken(i);
            break;

        case SymbolType::SPECIAL:
            if (currentSymbol.value == '"')
                currentToken = readStringToken(lexicalData.symbols, i);
            else if (currentSymbol.value == '\'')
                currentToken = readCharToken(lexicalData.symbols, i);
            else if (currentSymbol.value == ';')
                currentToken = buildOneSymbolToken(TokenType::ENDRULE, currentSymbol, i);
            else if (currentSymbol.value == '(')
                currentToken = buildOneSymbolToken(TokenType::START_ARGUMENT_LIST, currentSymbol, i);
            else if (currentSymbol.value == ')')
                currentToken = buildOneSymbolToken(TokenType::END_ARGUMENT_LIST, currentSymbol, i);
            else if (currentSymbol.value == ',')
                currentToken = buildOneSymbolToken(TokenType::ARGUMENTS_SEPARATOR, currentSymbol, i);
            else
                currentToken = readOperatorToken(lexicalData.symbols, i);
            pushCurrentToken(i);
            break;
        default:
            break;
        }
    }
    return lexicalData;
}

std::set<std::string> Lab2LexicalAnalyzer::getKeyWords()
{
    return keyWords;
}
std::set<char> Lab2LexicalAnalyzer::getSpecialSymbols()
{
    return specialSymbols;
}