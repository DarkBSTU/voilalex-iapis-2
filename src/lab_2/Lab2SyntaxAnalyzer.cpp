#include "Lab2SyntaxAnalyzer.h"
#include "../Voilang/Voilang.h"

void parseOneRule(const std::vector<Token>& tokens, size_t& currentPosition) {
    bool isArgumentsListStarted = false;
    bool isArgumentsListEnded = false;
    bool isReadyForEvaluable = false;
    bool isNamePresented = false;
    bool isParsingComplete = false;
    for (size_t& i = currentPosition; i < tokens.size(); i++) {
        Token token = tokens[i];
        if (token.type == TokenType::IDENTIFIER) {
            if (!isNamePresented)
                isNamePresented = true;
            else if (!isArgumentsListStarted || !isReadyForEvaluable)
                throw SyntaxError("Syntax error.");
            else
                isReadyForEvaluable = false;
        } else if (token.type == TokenType::START_ARGUMENT_LIST) {
            if (isNamePresented && !isArgumentsListStarted) {
                isArgumentsListStarted = true;
                isReadyForEvaluable = true;
            }
            else throw SyntaxError("Syntax error.");
        } else if (token.type == TokenType::STRING_CONSTANT ||
                   token.type == TokenType::CHAR_CONSTANT ||
                   token.type == TokenType::INTEGER_CONSTANT ||
                   token.type == TokenType::FLOAT_CONSTANT) {
            if (isReadyForEvaluable)
                isReadyForEvaluable = false;
            else throw SyntaxError("Syntax error.");
        } else if (token.type == TokenType::ARGUMENTS_SEPARATOR) {
            if (!isReadyForEvaluable)
                isReadyForEvaluable = true;
            else throw SyntaxError("Syntax error.");
        } else if (token.type == TokenType::END_ARGUMENT_LIST) {
            if (isArgumentsListStarted && !isReadyForEvaluable)
                isArgumentsListEnded = true;
            else throw SyntaxError("Syntax error.");
        } else if (token.type == TokenType::ENDRULE) {
            if (isArgumentsListEnded) {
                isParsingComplete = true;
                i++;
                break;
            }
            else throw SyntaxError("Syntax error.");
        }
    }
    if (!isParsingComplete)
        throw SyntaxError("Syntax error.");
    currentPosition--;
}

SyntaxData Lab2SyntaxAnalyzer::operator()(const LexicalData &lexicalData) {
    for (size_t i = 0; i < lexicalData.tokens.size(); i++)
        parseOneRule(lexicalData.tokens, i);
    return SyntaxData();
}
