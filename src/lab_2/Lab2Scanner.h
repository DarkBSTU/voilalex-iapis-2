#pragma once

#include "../Voilang/InteractiveScanner.h"

class Lab2Scanner : public InteractiveScanner {
public:
    InstructionScanner& operator >> (std::vector<Symbol>& buffer) override;
};