#include <iostream>
#include <boost/format.hpp>
#include "../Voilang/Voilang.h"
#include "Lab2LexicalAnalyzer.h"
#include "Lab2SyntaxAnalyzer.h"
#include "Lab2Scanner.h"

class MyApplication : public Application {
public:
    MyApplication()
            : Application(
            std::shared_ptr<InstructionScanner>(new Lab2Scanner()),
            std::shared_ptr<LexicalAnalyzer>(new Lab2LexicalAnalyzer()),
            std::shared_ptr<SyntaxAnalyzer>(new Lab2SyntaxAnalyzer())
    ) {}

    void useLexicalData(LexicalData& lexicalData) override {
        for (Token token : lexicalData.tokens) {
            std::cout << (boost::format("%1% - %2%\n") % token.value % toString(token.type)).str();
        }
    }
};

int IlyaMain(int argc, char **argv) {
    MyApplication app;
    app(argc, argv);
    return 0;
}
